INSERT INTO auditorium (id, name, numberOfSeats) VALUES (1, 'Auditorium 1', 10);
INSERT INTO auditorium (id, name, numberOfSeats) VALUES (2, 'Auditorium 2', 40);

INSERT INTO vipSeat (auditoriumId, seat) VALUES (1, 1);
INSERT INTO vipSeat (auditoriumId, seat) VALUES (1, 2);
INSERT INTO vipSeat (auditoriumId, seat) VALUES (1, 3);

INSERT INTO vipSeat (auditoriumId, seat) VALUES (2, 1);
INSERT INTO vipSeat (auditoriumId, seat) VALUES (2, 10);
INSERT INTO vipSeat (auditoriumId, seat) VALUES (2, 20);
INSERT INTO vipSeat (auditoriumId, seat) VALUES (2, 30);
