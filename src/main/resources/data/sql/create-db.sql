-- domain objects
CREATE TABLE user (
  id IDENTITY PRIMARY KEY NOT NULL,
  firstName VARCHAR(50),
  lastName VARCHAR(50),
  email VARCHAR(100),
  birthday DATE
);

CREATE TABLE event (
  id IDENTITY PRIMARY KEY NOT NULL,
  name VARCHAR(100),
  basePrice DOUBLE,
  rating VARCHAR(4)
);

CREATE TABLE ticket (
  id INTEGER PRIMARY KEY NOT NULL,
  userId BIGINT REFERENCES user(id),
  eventId BIGINT REFERENCES event(id),
  dateTime TIMESTAMP, -- DATETIME?
  seath BIGINT REFERENCES ticket(id),
  purchased BOOLEAN
);

CREATE TABLE auditorium (
  id IDENTITY PRIMARY KEY NOT NULL,
  name VARCHAR(100),
  numberOfSeats BIGINT
);

CREATE TABLE vipSeat (
  auditoriumId BIGINT REFERENCES auditorium(id),
  seat BIGINT
);

CREATE TABLE seance (
  eventId BIGINT REFERENCES event(id),
  auditoriumId BIGINT REFERENCES auditorium(id),
  date TIMESTAMP -- DATETIME?
);

-- aspects
CREATE TABLE counterForNameRequest (
  eventId BIGINT REFERENCES event(id),
  counter BIGINT
);

CREATE TABLE counterForPriceRequest (
  eventId BIGINT REFERENCES event(id),
  counter BIGINT
);

CREATE TABLE bookingCounter (
  eventId BIGINT REFERENCES event(id),
  counter BIGINT
);

CREATE TABLE luckyEvent (
  eventId BIGINT REFERENCES event(id),
  userId BIGINT REFERENCES user(id)
);

CREATE TABLE counterByDiscountStrategy (
  className VARCHAR(1000) PRIMARY KEY NOT NULL ,
  counter BIGINT
);

CREATE TABLE counterByUserAndDiscountStrategy (
  userId BIGINT REFERENCES user(id),
  className VARCHAR(1000) NOT NULL,
  counter BIGINT
);