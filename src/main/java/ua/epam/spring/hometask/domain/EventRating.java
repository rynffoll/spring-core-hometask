package ua.epam.spring.hometask.domain;

/**
 * @author Yuriy_Tkach
 */
public enum EventRating {
  LOW("LOW"),
  MID("MID"),
  HIGH("HIGH");

  private final String value;

  EventRating(String value) {
    this.value = value;
  }

  public String getValue() {
    return this.value;
  }

  @Override
  public String toString() {
    return this.getValue();
  }
}
