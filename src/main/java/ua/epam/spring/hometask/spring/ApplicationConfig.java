package ua.epam.spring.hometask.spring;

import org.springframework.context.annotation.*;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

/**
 * Created by rynffoll on 02.04.16.
 */
@Configuration
@PropertySource("classpath:application.properties")
@ComponentScan("ua.epam.spring.hometask")
@EnableAspectJAutoProxy
public class ApplicationConfig {

  @Bean
  public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
    return new PropertySourcesPlaceholderConfigurer();
  }
}
