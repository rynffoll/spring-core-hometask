package ua.epam.spring.hometask.strategy;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import ua.epam.spring.hometask.domain.Event;
import ua.epam.spring.hometask.domain.Ticket;
import ua.epam.spring.hometask.domain.User;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

/**
 * Created by rynffoll on 27.03.16.
 */
@Component
public class BirthdayDiscountStrategy implements DiscountStrategy {

  @Value("${birthdayDiscount.discount}")
  private byte discount;
  @Value("${birthdayDiscount.interval}")
  private byte interval;

  public byte getDiscount() {
    return discount;
  }

  public void setDiscount(byte discount) {
    this.discount = discount;
  }

  public byte getInterval() {
    return interval;
  }

  public void setInterval(byte interval) {
    this.interval = interval;
  }

  @Override
  public byte getDiscount(@Nullable User user, @Nonnull Event event, @Nonnull LocalDateTime airDateTime, long numberOfTickets) {

    byte currentDiscount = 0;

    if (user != null && user.getBirthday() != null) {
      LocalDate birthday = user.getBirthday();
      long daysLeft = ChronoUnit.DAYS.between(birthday, airDateTime);
      currentDiscount = (byte) ((daysLeft <= interval) ? discount : 0);
    }

    return currentDiscount;
  }
}
