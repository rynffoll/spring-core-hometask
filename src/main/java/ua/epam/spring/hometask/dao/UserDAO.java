package ua.epam.spring.hometask.dao;

import ua.epam.spring.hometask.domain.User;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Created by rynffoll on 27.03.16.
 */
public interface UserDAO extends AbstractDomainObjectDAO<User> {

  @Nullable User getUserByEmail(@Nonnull String email);

}
