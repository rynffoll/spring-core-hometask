package ua.epam.spring.hometask.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import ua.epam.spring.hometask.dao.TicketDAO;
import ua.epam.spring.hometask.domain.Ticket;
import ua.epam.spring.hometask.util.DateConverter;

import javax.annotation.Nonnull;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by rynffoll on 27.03.16.
 */
@Repository
public class TicketDAOImpl implements TicketDAO {

  @Autowired
  private NamedParameterJdbcTemplate jdbcTemplate;

  @Override
  public Ticket save(@Nonnull Ticket ticket) {
    try {

      Long id = ticket.getId();
      Long userId = (ticket.getUser() != null) ? ticket.getUser().getId() : null;
      Long eventId = ticket.getEvent().getId();
      LocalDateTime dateTime = ticket.getDateTime();
      Long seat = ticket.getSeat();
      boolean purchased = ticket.isPurchased();

      Map<String, ?> params = new HashMap<String, Object>() {{
        put("id", id);
        put("userId", userId);
        put("eventId", eventId);
        put("dateTime", dateTime);
        put("seat", seat);
        put("purchased", purchased);
      }};

      jdbcTemplate.update("INSERT INTO ticket (id, userId, eventId, dateTime, seat, purchased) " +
              "VALUES(:id, :userId, :eventId, :dataTime, :seat, :purchased)", params);

      return ticket;
    } catch (EmptyResultDataAccessException e) {
      return null;
    }
  }

  @Override
  public void remove(@Nonnull Ticket ticket) {
    jdbcTemplate.update("DELETE FROM ticket WHERE id = :id", new MapSqlParameterSource("id", ticket.getId()));
  }

  @Override
  public Ticket getById(@Nonnull Long id) {
    try {
      return jdbcTemplate.queryForObject("SELECT * FROM ticket WHERE id = :id", new MapSqlParameterSource("id", id), this::mapRow);
    } catch (EmptyResultDataAccessException e) {
      return null;
    }
  }

  @Override
  public Set<Ticket> getAll() {
    try {
      return jdbcTemplate.query("SELECT * FROM ticket", this::mapRow).stream().collect(Collectors.toSet());
    } catch (EmptyResultDataAccessException e) {
      return null;
    }
  }

  private Ticket mapRow(ResultSet resultSet, int rowNumber) throws SQLException {
    Long id = resultSet.getLong("id");
    Date date = resultSet.getDate("dateTime");
    LocalDateTime dateTime = null;
    if (date != null) {
      dateTime = DateConverter.toLocalDateTime(date);
    }
    Long seat = resultSet.getLong("seat");
    boolean purchased = resultSet.getBoolean("purchased");

    Ticket ticket = new Ticket(dateTime, seat);
    ticket.setPurchased(purchased);
    return null;
  }
}
