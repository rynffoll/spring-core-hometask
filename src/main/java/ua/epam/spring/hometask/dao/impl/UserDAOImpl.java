package ua.epam.spring.hometask.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import ua.epam.spring.hometask.dao.UserDAO;
import ua.epam.spring.hometask.domain.User;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by rynffoll on 27.03.16.
 */
@Repository
public class UserDAOImpl implements UserDAO {

  @Autowired
  private NamedParameterJdbcTemplate jdbcTemplate;

  @Nullable
  @Override
  public User getUserByEmail(@Nonnull String email) {
    try {
      SqlParameterSource params = new MapSqlParameterSource("email", email);
      return jdbcTemplate.queryForObject("SELECT * FROM user WHERE id = :id", params, this::mapUser);
    } catch (EmptyResultDataAccessException e) {
      return null;
    }
  }

  @Override
  public User save(@Nonnull User user) {
    try {
      SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(user);
      KeyHolder keyHolder = new GeneratedKeyHolder();

      jdbcTemplate.update("INSERT INTO user (firstName, lastName, email, birthday) " +
              "VALUES(:firstName, :lastName, :email, :birthday)", parameterSource, keyHolder);
      user.setId(keyHolder.getKey().longValue());

      return user;
    } catch (EmptyResultDataAccessException e) {
      return null;
    }
  }

  @Override
  public void remove(@Nonnull User user) {
    jdbcTemplate.update("DELETE FROM user WHERE id = :id", new MapSqlParameterSource("id", user.getId()));
  }

  @Override
  public User getById(@Nonnull Long id) {
    try {
      return jdbcTemplate.queryForObject("SELECT * FROM user WHERE id = :id", new MapSqlParameterSource("id", id), this::mapUser);
    } catch (EmptyResultDataAccessException e) {
      return null;
    }
  }

  @Override
  public Set<User> getAll() {
    try {
      Set<User> users = jdbcTemplate.query("SELECT * FROM user", this::mapUser).stream().collect(Collectors.toSet());
      return users;
    } catch (EmptyResultDataAccessException e) {
      return null;
    }
  }

  private User mapUser(ResultSet resultSet, int rowNumber) throws SQLException {
    User user = new User();
    user.setId(resultSet.getLong("id"));
    user.setFirstName(resultSet.getString("firstName"));
    user.setLastName(resultSet.getString("lastName"));
    user.setEmail(resultSet.getString("email"));
    Date birthday = resultSet.getDate("birthday");
    if (birthday != null) {
      user.setBirthday(birthday.toLocalDate());
    }
    return user;
  }
}
