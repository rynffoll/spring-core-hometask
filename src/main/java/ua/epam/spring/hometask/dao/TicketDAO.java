package ua.epam.spring.hometask.dao;

import ua.epam.spring.hometask.domain.Ticket;

/**
 * Created by rynffoll on 27.03.16.
 */
public interface TicketDAO extends AbstractDomainObjectDAO<Ticket> {

}
