package ua.epam.spring.hometask.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import ua.epam.spring.hometask.dao.AuditoriumDAO;
import ua.epam.spring.hometask.dao.EventDAO;
import ua.epam.spring.hometask.domain.Auditorium;
import ua.epam.spring.hometask.domain.Event;
import ua.epam.spring.hometask.util.DateConverter;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by rynffoll on 27.03.16.
 */
@Repository
public class EventDAOImpl implements EventDAO {

  @Autowired
  private NamedParameterJdbcTemplate jdbcTemplate;
  @Autowired
  private AuditoriumDAO auditoriumDAO;

  @Nullable
  @Override
  public Event getByName(@Nonnull String name) {
    try {
      Event event = jdbcTemplate.queryForObject(
              "SELECT * FROM event WHERE name = :name",
              new MapSqlParameterSource("name", name),
              this::mapRow);
      event.setAirDates(getAirDates(event));
      event.setAuditoriums(getAuditoriums(event));
      return event;
    } catch (EmptyResultDataAccessException e) {
      return null;
    }
  }

  @Override
  public Event save(@Nonnull Event event) {
    try {
      SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(event);
      KeyHolder keyHolder = new GeneratedKeyHolder();

      jdbcTemplate.update(
              "INSERT INTO event (name, basePrice, rating) VALUES(:name, :basePrice, :rating)",
              parameterSource,
              keyHolder);

      long id = keyHolder.getKey().longValue();
      event.setId(id);

      NavigableMap<LocalDateTime, Auditorium> auditoriums = event.getAuditoriums();
      for (Map.Entry<LocalDateTime, Auditorium> entry : auditoriums.entrySet()) {
        LocalDateTime dateTime = entry.getKey();
        Auditorium auditorium = entry.getValue();

        Map<String, ?> params = new HashMap<String, Object>() {{
          put("eventId", event.getId());
          put("auditoriumId", auditorium.getId());
          put("date", DateConverter.toDate(dateTime));
        }};

        jdbcTemplate.update(
                "INSERT INTO seance (eventId, auditoriumId, date) VALUES(:eventId, :auditoriumId, :date)",
                params);
      }

      return event;
    } catch (EmptyResultDataAccessException e) {
      return null;
    }
  }

  @Override
  public void remove(@Nonnull Event event) {
    jdbcTemplate.update("DELETE FROM event WHERE id = :id", new MapSqlParameterSource("id", event.getId()));
  }

  @Override
  public Event getById(@Nonnull Long id) {
    try {
      Event event = jdbcTemplate.queryForObject(
              "SELECT * FROM event WHERE id = :id",
              new MapSqlParameterSource("id", id),
              this::mapRow);
      NavigableSet<LocalDateTime> airDates = getAirDates(event);
      event.setAirDates(airDates);
      event.setAuditoriums(getAuditoriums(event));
      return event;
    } catch (EmptyResultDataAccessException e) {
      return null;
    }
  }

  @Override
  public Set<Event> getAll() {
    try {
      Set<Event> events = jdbcTemplate.query(
              "SELECT * FROM event",
              this::mapRow)
              .stream()
              .collect(Collectors.toSet());
      events.forEach(e -> {
        e.setAirDates(getAirDates(e));
        e.setAuditoriums(getAuditoriums(e));
      });
      return events;
    } catch (EmptyResultDataAccessException e) {
      return null;
    }
  }

  private NavigableSet<LocalDateTime> getAirDates(Event event) {
    try {
      Set<LocalDateTime> airDates = jdbcTemplate.queryForList(
              "SELECT date FROM seance WHERE eventId = :id",
              new MapSqlParameterSource("id", event.getId()),
              Date.class)
              .stream()
              .map(d -> DateConverter.toLocalDateTime(d))
              .collect(Collectors.toSet());

      NavigableSet<LocalDateTime> result = new TreeSet<>(airDates);
      return result;
    } catch (EmptyResultDataAccessException e) {
      return null;
    }
  }

  private NavigableMap<LocalDateTime, Auditorium> getAuditoriums(Event event) {
      NavigableMap<LocalDateTime, Auditorium> auditoriums = new TreeMap<>();
      NavigableSet<LocalDateTime> airDates = event.getAirDates();
      for (LocalDateTime airDate : airDates) {
          Map<String, ?> params = new HashMap<String, Object>() {{
            put("id", event.getId());
            put("date", DateConverter.toDate(airDate));
          }};
          Long auditoriumId = jdbcTemplate.queryForObject(
                  "SELECT auditoriumId FROM seance WHERE date = :date AND eventId = :id",
                  params,
                  Long.class);
          Auditorium auditorium = auditoriumDAO.getAll()
                  .stream()
                  .filter(a -> a.getId().equals(auditoriumId))
                  .findFirst()
                  .get();
          auditoriums.put(airDate, auditorium);
      }

      return auditoriums;
  }

  private Event mapRow(ResultSet resultSet, int rowNumber) throws SQLException {
    Event event = new Event();
    event.setId(resultSet.getLong("id"));
    event.setName(resultSet.getString("name"));
    event.setBasePrice(resultSet.getDouble("basePrice"));
    event.setRating(resultSet.getString("rating"));
    return event;
  }
}
