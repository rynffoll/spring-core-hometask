package ua.epam.spring.hometask.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import ua.epam.spring.hometask.dao.AuditoriumDAO;
import ua.epam.spring.hometask.domain.Auditorium;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by rynffoll on 27.03.16.
 */
@Repository
public class AuditoriumDAOImpl implements AuditoriumDAO {

  @Autowired
  private NamedParameterJdbcTemplate jdbcTemplate;

  @Nullable
  @Override
  public Auditorium getByName(@Nonnull String name) {
    try {
      Auditorium auditorium =jdbcTemplate.queryForObject(
              "SELECT * FROM auditorium WHERE name = :name",
              new MapSqlParameterSource("name", name),
              this::mapRow);
      Set<Long> vipSeats = jdbcTemplate.queryForList(
              "SELECT seat  FROM vipSeat WHERE auditoriumId = :auditoriumId",
              new MapSqlParameterSource("auditoriumId", auditorium.getId()),
              Long.class)
              .stream()
              .collect(Collectors.toSet());
      auditorium.setVipSeats(vipSeats);
      return auditorium;
    } catch (EmptyResultDataAccessException e) {
      return null;
    }
  }

  @Override
  public Set<Auditorium> getAll() {
    try {
      Set<Auditorium> auditoriumSet = jdbcTemplate.query(
              "SELECT * FROM auditorium",
              this::mapRow)
              .stream()
              .collect(Collectors.toSet());

      for (Auditorium auditorium : auditoriumSet) {
        Set<Long> vipSeats = jdbcTemplate.queryForList(
                "SELECT seat  FROM vipSeat WHERE auditoriumId = :auditoriumId",
                new MapSqlParameterSource("auditoriumId", auditorium.getId()),
                Long.class)
                .stream()
                .collect(Collectors.toSet());
        auditorium.setVipSeats(vipSeats);
      }

      return auditoriumSet;
    } catch (EmptyResultDataAccessException e) {
      return null;
    }
  }

  private Auditorium mapRow(ResultSet resultSet, int rowNumber) throws SQLException {
    Auditorium auditorium = new Auditorium();
    auditorium.setId(resultSet.getLong("id"));
    auditorium.setName(resultSet.getString("name"));
    auditorium.setNumberOfSeats(resultSet.getLong("numberOfSeats"));

    return auditorium;
  }
}
