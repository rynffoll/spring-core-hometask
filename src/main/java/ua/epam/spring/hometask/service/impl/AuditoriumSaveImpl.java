package ua.epam.spring.hometask.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.epam.spring.hometask.dao.AuditoriumDAO;
import ua.epam.spring.hometask.domain.Auditorium;
import ua.epam.spring.hometask.service.AuditoriumService;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Set;

/**
 * Created by rynffoll on 27.03.16.
 */
@Service
public class AuditoriumSaveImpl implements AuditoriumService {

  @Autowired
  private AuditoriumDAO auditoriumDAO;

  public AuditoriumDAO getAuditoriumDAO() {
    return auditoriumDAO;
  }

  public void setAuditoriumDAO(AuditoriumDAO auditoriumDAO) {
    this.auditoriumDAO = auditoriumDAO;
  }

  @Nonnull
  @Override
  public Set<Auditorium> getAll() {
    return auditoriumDAO.getAll();
  }

  @Nullable
  @Override
  public Auditorium getByName(@Nonnull String name) {
    return auditoriumDAO.getByName(name);
  }
}
