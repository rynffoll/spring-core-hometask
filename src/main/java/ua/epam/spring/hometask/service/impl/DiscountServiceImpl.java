package ua.epam.spring.hometask.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.epam.spring.hometask.dao.EventDAO;
import ua.epam.spring.hometask.dao.UserDAO;
import ua.epam.spring.hometask.domain.Event;
import ua.epam.spring.hometask.domain.User;
import ua.epam.spring.hometask.service.DiscountService;
import ua.epam.spring.hometask.strategy.DiscountStrategy;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by rynffoll on 27.03.16.
 */
@Service
public class DiscountServiceImpl implements DiscountService {

  private List<DiscountStrategy> discountStrategies;

  @Autowired
  public DiscountServiceImpl(List<DiscountStrategy> discountStrategies) {
    this.discountStrategies = discountStrategies;
  }

  public List<DiscountStrategy> getDiscountStrategies() {
    return discountStrategies;
  }

  public void setDiscountStrategies(List<DiscountStrategy> discountStrategies) {
    this.discountStrategies = discountStrategies;
  }

  @Override
  public byte getDiscount(@Nullable User user, @Nonnull Event event, @Nonnull LocalDateTime airDateTime, long numberOfTickets) {
    return discountStrategies
            .stream()
            .map(discountStrategy -> discountStrategy.getDiscount(user, event, airDateTime, numberOfTickets))
            .max(Byte::compare)
            .get();
  }
}
