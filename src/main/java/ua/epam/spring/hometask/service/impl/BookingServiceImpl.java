package ua.epam.spring.hometask.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ua.epam.spring.hometask.dao.EventDAO;
import ua.epam.spring.hometask.dao.TicketDAO;
import ua.epam.spring.hometask.dao.UserDAO;
import ua.epam.spring.hometask.domain.*;
import ua.epam.spring.hometask.service.BookingService;
import ua.epam.spring.hometask.service.DiscountService;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by rynffoll on 27.03.16.
 */
@Service
public class BookingServiceImpl implements BookingService {

  @Value("${bookingService.markupForHighRatingEvent}")
  private double markupForHighRatingEvent;
  @Value("${bookingService.markupForVipSeat}")
  private double markupForVipSeat;

  @Autowired
  private DiscountService discountService;
  @Autowired
  private TicketDAO ticketDAO;
  @Autowired
  private EventDAO eventDAO;
  @Autowired
  private UserDAO userDAO;

  @Override
  public double getTicketsPrice(@Nonnull Event event, @Nonnull LocalDateTime dateTime, @Nullable User user, @Nonnull Set<Long> seats) {
    double totalPrice = 0;

    long numberOfTickets = seats.size();
    byte discount = discountService.getDiscount(user, event, dateTime, numberOfTickets);

    double basePrice = event.getBasePrice();
    String rating = event.getRating();
    double currentPrice = (rating.equals(EventRating.HIGH)) ? basePrice * markupForHighRatingEvent : basePrice;

    Auditorium auditorium = event.getAuditoriums().entrySet()
            .stream()
            .filter(entry -> entry.getKey().equals(dateTime))
            .map(Map.Entry::getValue)
            .findFirst()
            .get();
    Set<Long> vipSeats = auditorium.getVipSeats();

    totalPrice = seats
            .stream()
            .mapToDouble(seat -> (vipSeats.contains(seat) ? basePrice * markupForVipSeat : currentPrice))
            .sum();

    totalPrice /= (discount > 0) ? ((double) discount / 100.0) : 1.0;
    return totalPrice;
  }

  @Override
  public void bookTickets(@Nonnull Set<Ticket> tickets) {
    for (Ticket ticket : tickets) {
      ticket.setPurchased(true);
      User user = ticket.getUser();
      if (user != null && user.getId() != null) {
        user.getTickets().add(ticket);
      }
    }
  }

  @Nonnull
  @Override
  public Set<Ticket> getPurchasedTicketsForEvent(@Nonnull Event event, @Nonnull LocalDateTime dateTime) {
    return ticketDAO.getAll()
            .stream()
            .filter(ticket -> ticket.isPurchased() && ticket.getEvent().equals(event) && ticket.getDateTime().equals(dateTime))
            .collect(Collectors.toSet());
  }
}
