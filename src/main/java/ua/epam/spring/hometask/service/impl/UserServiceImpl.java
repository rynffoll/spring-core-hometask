package ua.epam.spring.hometask.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.epam.spring.hometask.dao.TicketDAO;
import ua.epam.spring.hometask.dao.UserDAO;
import ua.epam.spring.hometask.domain.Ticket;
import ua.epam.spring.hometask.domain.User;
import ua.epam.spring.hometask.service.UserService;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.*;

/**
 * Created by rynffoll on 27.03.16.
 */
@Service
public class UserServiceImpl implements UserService {

  @Autowired
  private UserDAO userDAO;

  @Autowired
  private TicketDAO ticketDAO;

  public UserDAO getUserDAO() {
    return userDAO;
  }

  public void setUserDAO(UserDAO userDAO) {
    this.userDAO = userDAO;
  }

  @Nullable
  @Override
  public User getUserByEmail(@Nonnull String email) {
    User user = userDAO.getUserByEmail(email);
    user.setTickets(getTicketsByUser(user));
    return user;
  }

  @Override
  public User save(@Nonnull User user) {
    return userDAO.save(user);
  }

  @Override
  public void remove(@Nonnull User user) {
    userDAO.remove(user);
  }

  @Override
  public User getById(@Nonnull Long id) {
    User user = userDAO.getById(id);
    user.setTickets(getTicketsByUser(user));
    return user;
  }

  @Nonnull
  @Override
  public Collection<User> getAll() {
    Collection<User> users =  userDAO.getAll();
    users.forEach(u -> u.setTickets(getTicketsByUser(u)));
    return users;
  }

  private NavigableSet<Ticket> getTicketsByUser(User user) {
    NavigableSet<Ticket> tickets = new TreeSet<>();
    ticketDAO.getAll()
            .stream()
            .filter(t -> t.getUser().getId().equals(user.getId()))
            .forEach(t -> tickets.add(t));
    return tickets;
  }
}
