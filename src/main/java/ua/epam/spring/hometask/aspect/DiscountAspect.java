package ua.epam.spring.hometask.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.epam.spring.hometask.aspect.dao.DiscountDAO;
import ua.epam.spring.hometask.domain.User;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by rynffoll on 02.04.16.
 */
@Aspect
@Component
public class DiscountAspect {

  @Autowired
  private DiscountDAO discountDAO;

  @AfterReturning(
          pointcut = "execution(byte ua.epam.spring.hometask.strategy.*.getDiscount(..))",
          returning = "result"
  )
  public void countDiscount(JoinPoint joinPoint, byte result) {
    Class<?> discountStrategyClass = joinPoint.getTarget().getClass();
    Object[] args = joinPoint.getArgs();
    User user = (User) args[0];

    boolean hasDiscount = result > 0;
    if (hasDiscount) {
      Integer counterByClass = discountDAO.getCounterByDiscountStrategy(discountStrategyClass.getName());
      discountDAO.updateCounterByDiscountStrategy(discountStrategyClass.getName(), counterByClass + 1);

      System.out.println("[ASPECT] Counter by strategy: " + counterByClass);

      if (user != null) {
        Integer counter = discountDAO.getCounterByUserAndDiscountStrategy(user, discountStrategyClass.getName());
        discountDAO.updateCounterByUserAndDiscountStrategy(user, discountStrategyClass.getName(), counter + 1);

        System.out.println("[ASPECT] Counter by strategy and specific user: " + counter);
      }
    }
  }

}
