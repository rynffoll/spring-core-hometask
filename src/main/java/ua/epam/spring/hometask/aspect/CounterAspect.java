package ua.epam.spring.hometask.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.epam.spring.hometask.aspect.dao.CounterDAO;
import ua.epam.spring.hometask.domain.Event;
import ua.epam.spring.hometask.domain.Ticket;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by rynffoll on 02.04.16.
 */
@Aspect
@Component
public class CounterAspect {

  @Autowired
  private CounterDAO counterDAO;

  @Pointcut("execution(* ua.epam.spring.hometask.service.impl.EventServiceImpl.getByName(..))")
  public void callGetEventByName() {}

  @Pointcut("execution(* ua.epam.spring.hometask.service.BookingService.getTicketsPrice(..))")
  public void callGetEventPrice() {}

  @Pointcut("execution(* ua.epam.spring.hometask.service.impl.BookingServiceImpl.bookTickets(..))")
  public void callBookTickets() {}

  @AfterReturning(
          pointcut = "callGetEventByName()",
          returning = "event"
  )
  public void countCallGetEventByName(Event event) {
    if (event != null) {
      Integer counter = counterDAO.getCounterForNameRequest(event);
      counterDAO.updateForNameRequest(event, counter + 1);

      System.out.println("[ASPECT] Counter for event name request: " + counter);
    }
  }

  @AfterReturning("callGetEventPrice()")
  public void countCallGetEventPrice(JoinPoint joinPoint) {
    Object[] args = joinPoint.getArgs();
    Event event = (Event) args[0];
    if (event != null) {
      Integer counter = counterDAO.getCounterForPriceRequest(event);
      counterDAO.updateForPriceRequest(event, counter + 1);

      System.out.println("[ASPECT] Counter for price request: " + counter);
    }
  }

  @AfterReturning("callBookTickets()")
  public void countCallBookTickets(JoinPoint joinPoint) {
    Object[] args = joinPoint.getArgs();
    Set<Ticket> tickets = (Set<Ticket>) args[0];
    tickets.forEach(ticket -> {
      Event event = ticket.getEvent();
      if (event != null) {
        Integer counter = counterDAO.getCounterBookingCounter(event);
        counterDAO.updateBookingCounter(event, counter + 1);

        System.out.println("[ASPECT] Booking counter: " + counter);
      }
    });
  }
}
