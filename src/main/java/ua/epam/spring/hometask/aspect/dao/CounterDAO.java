package ua.epam.spring.hometask.aspect.dao;

import ua.epam.spring.hometask.domain.Event;

/**
 * Created by rynffoll on 13.04.16.
 */
public interface CounterDAO {
  void updateForNameRequest(Event event, Integer counter);
  void updateForPriceRequest(Event event, Integer counter);
  void updateBookingCounter(Event event, Integer counter);

  int getCounterForNameRequest(Event event);
  int getCounterForPriceRequest(Event event);
  int getCounterBookingCounter(Event event);
}
