package ua.epam.spring.hometask.aspect.dao;

import ua.epam.spring.hometask.domain.Event;
import ua.epam.spring.hometask.domain.User;

import java.util.Set;

/**
 * Created by rynffoll on 10.04.16.
 */
public interface LuckyEventDAO {

  void save(User user, Event event);

}
