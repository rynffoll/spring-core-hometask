package ua.epam.spring.hometask.aspect.dao;

import ua.epam.spring.hometask.domain.User;

/**
 * Created by rynffoll on 13.04.16.
 */
public interface DiscountDAO {
  int getCounterByDiscountStrategy(String className);
  int getCounterByUserAndDiscountStrategy(User user, String className);

  void updateCounterByDiscountStrategy(String className, Integer counter);
  void updateCounterByUserAndDiscountStrategy(User user, String className, Integer counter);
}
