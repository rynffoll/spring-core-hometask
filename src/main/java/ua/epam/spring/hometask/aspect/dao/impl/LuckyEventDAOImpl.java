package ua.epam.spring.hometask.aspect.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import ua.epam.spring.hometask.aspect.dao.LuckyEventDAO;
import ua.epam.spring.hometask.domain.Event;
import ua.epam.spring.hometask.domain.User;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by rynffoll on 13.04.16.
 */
@Repository
public class LuckyEventDAOImpl implements LuckyEventDAO {

  @Autowired
  private NamedParameterJdbcTemplate jdbcTemplate;

  @Override
  public void save(User user, Event event) {
    Map<String, ?> params = new HashMap<String, Object>() {{
      put("userId", user.getId());
      put("eventId", event.getId());
    }};
    jdbcTemplate.update(
            "INSERT INTO luckyEvent (userId, eventId) VALUES(:userId, :eventId)",
            params);
  }
}
